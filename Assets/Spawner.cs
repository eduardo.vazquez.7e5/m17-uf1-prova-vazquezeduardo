using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemy;
    private float TimeToSpawn = 3f;
    private float TimeToSpawnCurrent = 0f;

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

    // Start is called before the first frame update
    void Start()
    {

        var lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        var upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        minX = lowerLeft.x;
        maxX = upperRight.x;
        maxY = upperRight.y;
        minY = lowerLeft.y;
    }

    // Update is called once per frame
    void Update()
    {
        TimeToSpawnCurrent += Time.deltaTime;
        if(TimeToSpawnCurrent >= TimeToSpawn)
        {
            TimeToSpawnCurrent -= TimeToSpawn;
            var clone = Instantiate(enemy);
            clone.GetComponent<Transform>().position = new Vector3(Random.Range(minX, maxX), maxY + 2f, 0);
        }
    }
}
