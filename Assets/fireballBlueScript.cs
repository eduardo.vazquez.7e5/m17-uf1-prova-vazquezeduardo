using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireballBlueScript : MonoBehaviour
{
    public float _speed = 5f;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    // Start is called before the first frame update
    void Start()
    {
        var lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        var upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        minX = lowerLeft.x;
        maxX = upperRight.x;
        maxY = upperRight.y;
        minY = lowerLeft.y;
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Transform>().position += _speed * new Vector3(0, 1, 0)*Time.deltaTime;
        if (GetComponent<Transform>().position.y > maxY+5f) Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Finish"))
        {
            Destroy(col.gameObject);
            ScoreManager.Instance.Score += 5;
        }
        if (col.CompareTag("BigBoss"))
        {
            if (--col.gameObject.GetComponent<FragateScript>().lives <= 0)
            {
                Destroy(col.gameObject);
                ScoreManager.Instance.Score += 20;
            }
            else
            {
                Destroy(this.gameObject);
                col.gameObject.GetComponent<FragateScript>().Hide = true;
            }
        }
    }
}
