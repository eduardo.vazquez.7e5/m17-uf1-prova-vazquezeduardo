using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightOh : MonoBehaviour
{
    public float _speed = 2f;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    private Vector3 movement;
    // Start is called before the first frame update
    void Start()
    {
        var lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        var upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        minX = lowerLeft.x;
        maxX = upperRight.x;
        maxY = upperRight.y;
        minY = lowerLeft.y;

        Vector3 player = GameObject.Find("PlayerShip").GetComponent<Transform>().position;
        movement = _speed * (player - GetComponent<Transform>().position).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Transform>().position += movement * Time.deltaTime;
        if (GetComponent<Transform>().position.y < minY - 5f) Destroy(this.gameObject);
    }
}
