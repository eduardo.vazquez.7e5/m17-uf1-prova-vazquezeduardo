using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerShip : MonoBehaviour
{
    private float maxVelocity = 7f;
    private float _acceleration = 0.3f;
    private Rigidbody2D _rb;
    private Transform _transform;
    public GameObject shotPrefab;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

    private bool isShooting = false;
    private bool isReloading = false;
    private int maxBullets = 10;
    private int currentBullets = 10;
    private float reloadTime = 0.3f;
    private float currentReloadTime = 0f;
    private float timeBetweenBullets = 0.5f;
    private float timeBetweenBulletsCurrent = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();

        var lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        var upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        minX = lowerLeft.x;
        maxX = upperRight.x;
        maxY = upperRight.y;
        minY = lowerLeft.y;

    }

    // Update is called once per frame
    void Update()
    {
        MouLaNau();
        CrashAgainstLimits();
        Dispara();
    }

    void MouLaNau()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector2 input = new Vector2(x, y);
        if (!IsGoingMaxSpeed(input))
        {
            _rb.AddForce(_acceleration * input);
        }
        else
        {
            Vector2 accelerationForce = _acceleration * input;
            accelerationForce -= Vector2.Dot(accelerationForce, _rb.velocity) / Mathf.Pow(_rb.velocity.magnitude, 2) * _rb.velocity;
            _rb.AddForce(accelerationForce);
        }
    }

    void CrashAgainstLimits()
    {
        if(_transform.position.x > maxX)
        {
            _transform.position = new Vector3(maxX, _transform.position.y, 0);
            _rb.velocity = new Vector2(0, _rb.velocity.y);
        }
        if (_transform.position.x < minX)
        {
            Debug.Log("OJO");
            _transform.position = new Vector3(minX, _transform.position.y, 0);
            _rb.velocity = new Vector2(0, _rb.velocity.y);
        }
        if (_transform.position.y > maxY)
        {
            _transform.position = new Vector3(_transform.position.x, maxY, 0);
            _rb.velocity = new Vector2(_rb.velocity.x,0);
        }
        if (_transform.position.y < minY)
        {
            _transform.position = new Vector3(_transform.position.x, minY, 0);
            _rb.velocity = new Vector2(_rb.velocity.x, 0);
        }
    }

    bool IsGoingMaxSpeed(Vector2 input)
    {
        return _rb.velocity.magnitude > maxVelocity && Vector2.Dot(_rb.velocity, input) > 0;
    }

    void Dispara()
    { 
        if (Input.GetKeyDown(KeyCode.Space) && !isReloading){
            isShooting = true;
        }
        if (Input.GetKeyUp(KeyCode.Space) || isReloading)
        {
            isShooting = false;
        }

        if (isShooting)
        {
            timeBetweenBulletsCurrent += Time.deltaTime;
            if (timeBetweenBulletsCurrent >= timeBetweenBullets)
            {
                timeBetweenBulletsCurrent -= timeBetweenBullets;
                var clone = Instantiate(shotPrefab, _transform.position, Quaternion.identity);
                clone.GetComponent<SpriteRenderer>().flipY = true;
                currentBullets--;
                if (currentBullets == 0) isReloading = true;

            }
        }
        else
        {
            currentReloadTime += Time.deltaTime;
            if(currentReloadTime >= reloadTime)
            {
                currentReloadTime = 0;
                currentBullets++;
                if (currentBullets > maxBullets)
                {
                    currentBullets = maxBullets;
                    isReloading = false;
                    timeBetweenBulletsCurrent = timeBetweenBullets;
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Finish") || col.CompareTag("BigBoss"))
        {
            SceneManager.LoadScene(0);
        }
    }
}
