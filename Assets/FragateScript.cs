using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateScript : MonoBehaviour
{
    public float _speed = 1f;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    private Vector3 movement;
    public GameObject shotPrefab;
    private Transform _transform;
    private float timeBetweenBulletsCurrent = 0f;
    private float timeBetweenBullets = 1f;

    public bool Hide = false;
    public float timeHide = 0.2f;
    public float timeHideCurrent = 0f;
    public int lives = 5;
    // Start is called before the first frame update
    void Start()
    {
        var lowerLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        var upperRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        minX = lowerLeft.x;
        maxX = upperRight.x;
        maxY = upperRight.y;
        minY = lowerLeft.y;

        movement = _speed * (new Vector3(0,-1,0)).normalized;
        _transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Transform>().position += movement * Time.deltaTime;
        if (GetComponent<Transform>().position.y < minY - 10f) Destroy(this);
        Dispara();

        if (Hide)
        {
            GetComponent<Renderer>().enabled = false;
            timeHideCurrent += Time.deltaTime;
            if(timeHideCurrent >= timeHide)
            {
                GetComponent<Renderer>().enabled = true;
                timeHideCurrent = 0f;
                Hide = false;
            }
        }
    }

    void Dispara()
    {
        timeBetweenBulletsCurrent += Time.deltaTime;
        if (timeBetweenBulletsCurrent >= timeBetweenBullets)
        {
            timeBetweenBulletsCurrent -= timeBetweenBullets;
            var clone = Instantiate(shotPrefab, _transform.position, Quaternion.identity);
        }
    }
}
